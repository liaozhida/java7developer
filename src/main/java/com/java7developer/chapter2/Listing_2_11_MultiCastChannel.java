package com.java7developer.chapter2;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.net.StandardProtocolFamily;
import java.net.StandardSocketOptions;
import java.nio.channels.DatagramChannel;
import java.nio.channels.MembershipKey;

/**
 * @author zhidaliao
 *
 *         网络多播： 表示一对多的网络，基本前提是将一个包发送到一个组播地址， 然后网络对包进行复制，发送给所有的接收者
 */
public class Listing_2_11_MultiCastChannel {

	public static void main(String[] args) {
		try {

			NetworkInterface networkInterface = NetworkInterface.getByName("net1");
			DatagramChannel dc = DatagramChannel.open(StandardProtocolFamily.INET);

			// 将通道设置为多播
			dc.setOption(StandardSocketOptions.SO_REUSEADDR, true);
			dc.bind(new InetSocketAddress(8080));
			dc.setOption(StandardSocketOptions.IP_MULTICAST_IF, networkInterface);

			// 加入多播组
			InetAddress group = InetAddress.getByName("180.90.4.12");
			MembershipKey key = dc.join(group, networkInterface);

		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}
}
