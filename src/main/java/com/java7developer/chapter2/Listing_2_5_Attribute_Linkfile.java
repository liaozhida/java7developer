package com.java7developer.chapter2;

import static java.nio.file.attribute.PosixFilePermission.GROUP_READ;
import static java.nio.file.attribute.PosixFilePermission.OWNER_EXECUTE;
import static java.nio.file.attribute.PosixFilePermission.OWNER_READ;
import static java.nio.file.attribute.PosixFilePermission.OWNER_WRITE;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileAttribute;
import java.nio.file.attribute.PosixFileAttributes;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import com.java7developer.extras.CurrentPath;

/**
 * Code for Listing 2_5 - You'll need to change the hardcoded path.
 * 
 * 修改文件属性
 */
public class Listing_2_5_Attribute_Linkfile {

	public static void main(String[] args) throws IOException, InterruptedException {

		test();
		System.out.println("~~~~~");
		createFile();
		System.out.println("~~~~~");
		form();
		System.out.println("~~~~~");
		link();
		testAttr();
		

		
	}

	static void createFile() throws IOException {

		// 创建文件
		Path zip = Paths.get(CurrentPath.cur()+"IO/hello.md");
		if (!Files.exists(zip)) {
			zip = Files.createFile(zip);
		}

		System.out.println(zip.getFileName());

		// 读取文件属性
		Map<String, Object> map = Files.readAttributes(zip, "*");
		System.out.println(map);

		// 读取文件 读写属性
		PosixFileAttributes mm = Files.readAttributes(zip, PosixFileAttributes.class);
		Set<PosixFilePermission> st = mm.permissions();
		Iterator<PosixFilePermission> it = st.iterator();
		while (it.hasNext()) {
			PosixFilePermission pfp = it.next();
			System.out.println(pfp.toString() + "~>" + pfp.ordinal() + "~> " + pfp.name());
		}

	}
	
	
	/**
	 * 读取文件属性
	 */
	static void test() {
		try {
			Path zip = Paths.get("/Users/zhidaliao/git");
			System.out.println(zip.toAbsolutePath().toString());
			System.out.println(Files.getLastModifiedTime(zip));
			System.out.println(Files.size(zip));
			System.out.println(Files.isSymbolicLink(zip));
			System.out.println(Files.isDirectory(zip));
			System.out.println(Files.readAttributes(zip, "*"));
		} catch (IOException ex) {
			System.out.println("Exception" + ex.getMessage());
		}
	}
	
	static void testAttr() {

		try {
			Path profile = Paths.get("/Users/zhidaliao/foo.log");

			PosixFileAttributes attrs = Files.readAttributes(profile, PosixFileAttributes.class);

			Set<PosixFilePermission> posixPermissions = attrs.permissions();
			posixPermissions.clear();

			String owner = attrs.owner().getName();
			String perms = PosixFilePermissions.toString(posixPermissions);
			System.out.format("%s %s%n", owner, perms);

			posixPermissions.add(OWNER_READ);
			posixPermissions.add(GROUP_READ);
			posixPermissions.add(OWNER_EXECUTE);
			posixPermissions.add(OWNER_WRITE);
			Files.setPosixFilePermissions(profile, posixPermissions);

		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}

	/**
	 * PosixFilePermissions 是一个工具类 、 PosixFilePermission是普通类
	 * 
	 * @throws IOException
	 * @throws InterruptedException
	 */
	static void form() throws IOException, InterruptedException {

		Path profile = Paths.get("/Users/zhidaliao/foo4.log");

		if (!profile.toFile().exists()) {

			Set<PosixFilePermission> posixPermissions = PosixFilePermissions.fromString("rwxrwxrwx");
			FileAttribute<Set<PosixFilePermission>> fa = PosixFilePermissions.asFileAttribute(posixPermissions);
			Path p = Files.createFile(profile, fa);
			System.out.println("Done ! ");

			// Thread.sleep(4000);
			//
			// Files.delete(profile);
		}

	}

	/**
	 * 链接文件
	 * 
	 * @throws IOException
	 */
	static void link() throws IOException {

		Path profile = Paths.get("/Users/zhidaliao/link.log.link").toAbsolutePath();
		System.out.println(Files.isSymbolicLink(profile));

		// 虽然读取到了源文件 但是文件目录已经被修改
		if (Files.isSymbolicLink(profile)) {
			profile = Files.readSymbolicLink(profile);
			System.out.println(profile.getFileName());
			System.out.println(profile.toUri());

			// 转换成正常的目录
			Path parentPath = Paths.get("/Users/zhidaliao");
			profile = profile.getFileName();
			profile = parentPath.resolve(profile);
			System.out.println(profile.toUri());
		}

		BasicFileAttributes basic = Files.readAttributes(profile, BasicFileAttributes.class, LinkOption.NOFOLLOW_LINKS);
		System.out.println(basic.isSymbolicLink());
	}

}
