package com.java7developer.chapter2;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.util.List;

/**
 * Code for Listing 2_4 - You'll need to change the hardcoded path.
 */
public class Listing_2_4_WirteRead_Move_eg {

	public static void main(String[] args) throws IOException {

		System.out.println("=============================================== >");

		oper();
		
		System.out.println("=============================================== >");

		readTest("/Users/zhidaliao/foo.log");

		System.out.println("=============================================== >");

		writeTest();
	}
	
	static void oper() throws IOException {

		Path profile = Paths.get("/Users/zhidaliao/foo.log");
		Path profile2 = Paths.get("/Users/zhidaliao/foo5.log");
		// 默认不能覆盖已存在的文件 除非加上 REPLACE_EXISTING
		// 还有 ATOMIC_MOVE COPY_ATTRIBUTES
		Files.copy(profile, profile2, StandardCopyOption.REPLACE_EXISTING);

	}

	/**
	 * 读取文件
	 * 
	 * @throws IOException
	 */
	static void readTest(String pathStr) throws IOException {

		Path path = Paths.get(pathStr);
		BufferedReader br = Files.newBufferedReader(path, StandardCharsets.UTF_8);

		String lineStr;
		while ((lineStr = br.readLine()) != null) {
			System.out.println(lineStr);
		}
		
		// 新版的读取
		List<String> lines = Files.readAllLines(path, StandardCharsets.UTF_8);
		for(String str: lines){
			System.out.println(str);
		}
	}

	/**
	 * 需要手动 flush ; StandardOpenOption 注意这个属性
	 * 
	 * @throws IOException
	 */
	static void writeTest() throws IOException {
		Path path = Paths.get("/Users/zhidaliao/foo.wr");
		BufferedWriter bw = Files.newBufferedWriter(path, StandardCharsets.UTF_8, StandardOpenOption.APPEND);
		bw.write("liaozhida");
		bw.flush();

		readTest(path.toString());
	}
	
	
	
}
