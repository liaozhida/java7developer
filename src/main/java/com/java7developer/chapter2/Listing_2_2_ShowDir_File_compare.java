package com.java7developer.chapter2;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import com.java7developer.extras.CurrentPath;

/**
 * Code for Listing 2_2 - You'll need to change the hardcoded path.
 */
public class Listing_2_2_ShowDir_File_compare {

	public static void main(String[] args) {

		// 遍历目录
		Path dir = Paths.get(CurrentPath.cur() + "IO");
		try (DirectoryStream<Path> stream = Files.newDirectoryStream(dir, "*")) {
			for (Path entry : stream) {
				System.out.println(entry.getFileName());
			}
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}

		// 计算两个文件的路径比较
		Path dir2 = Paths.get("/Users/zhidaliao/foo.log");
		Path dir3 = Paths.get("/Users/zhidaliao/git/jira_erp");
		Path rela = dir2.relativize(dir3);
		System.out.println(rela);

		// 相互转换
		File file = dir2.toFile();
		Path filePath = file.toPath();

	}

}
