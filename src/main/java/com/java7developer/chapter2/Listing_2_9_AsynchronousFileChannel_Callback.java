package com.java7developer.chapter2;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousFileChannel;
import java.nio.channels.CompletionHandler;
import java.nio.file.Path;
import java.nio.file.Paths;

import com.java7developer.extras.CurrentPath;

/**
 * Code for Listing 2_9 - You'll need to change the hardcoded path.
 */
public class Listing_2_9_AsynchronousFileChannel_Callback {
	
	private static volatile boolean stop = false;

	public static void main(String[] args) throws InterruptedException {

		try {

			Path file = Paths.get(CurrentPath.cur() + "IO/foo.log");

			System.out.println(file.toUri());

			AsynchronousFileChannel channel = AsynchronousFileChannel.open(file);

			ByteBuffer buffer = ByteBuffer.allocate(100000);

			// 回调式的写法  当主线程结束的时候 回调也会结束
			channel.read(buffer, 0, buffer, new CompletionHandler<Integer, ByteBuffer>() {

				public void completed(Integer result, ByteBuffer attachment) {
					System.out.println("Bytes read [" + result + "]");
					stop = true;
				}

				public void failed(Throwable exception, ByteBuffer attachment) {
					System.out.println(exception.getMessage());
					stop = true;
				}
			});
			
			
			while(!stop){
				// 避免主线程中断
			}
			
			
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}
}
