package com.java7developer.extras;

import java.nio.file.Path;
import java.nio.file.Paths;

public class CurrentPath {

	
	
	/**返回resource的路径
	 * @return
	 */
	public static String cur(){
		
		Path currentPath = Paths.get("./").toAbsolutePath();
		
		return currentPath.toString().substring(0, currentPath.toString().length()-1) + "src/main/resources/";
	}
}
